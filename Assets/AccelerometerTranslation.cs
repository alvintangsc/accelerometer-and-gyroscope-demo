﻿using UnityEngine;
using System.Collections;

public class AccelerometerTranslation : MonoBehaviour 
{
    public bool isDemoGyro;

	// Use this for initialization
	void Start () 
	{
        if (isDemoGyro)
            Input.gyro.enabled = true;	
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (isDemoGyro)
        {
            //transform.rotation = GyroToUnity(Input.gyro.attitude);
            transform.Rotate(Input.gyro.rotationRateUnbiased.x, Input.gyro.rotationRateUnbiased.y, 0);
        }
        else
        {
            transform.Translate(Input.acceleration.x * Time.deltaTime * 10, Input.acceleration.y * Time.deltaTime * 10, 0);

            ClampTranslation();
        }
	}

    private Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    void ClampTranslation ()
	{
		// Get the lower-left world coordinate of the viewport.
		Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
		
		// Get the upper-right world coordinate of the viewport.
		Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
		
		// Get the coordinates of the player
		Vector2 pos = transform.position;
		
		// Restrict the player from moving outside of the screen.
		pos.x = Mathf.Clamp (pos.x, min.x, max.x);
		pos.y = Mathf.Clamp (pos.y, min.y, max.y);
		
		// Assign the newly-restricted position value.
		transform.position = pos;
       
	}
}
